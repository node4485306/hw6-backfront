import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

function MainPage() {
    // створюємо стан застосунку
    // posts - масив з постами
    const [posts, setPosts] = useState([]);
    // page - поточна сторінка
    const [page, setPage] = useState(1);
    // count - кількість всіх постів
    const [count, setCount] = useState(0);
    // limit - кількість постів на сторінці
    const limit = 4;

    
    // функція яка виконується при зміні сторінки та перезавантаженні застосунку
    useEffect(() => {

        // функція яка завантажує пости з бази даних
        const loadPosts = async () => {
            // отримуємо скільки постів потрібно пропустити на основі поточної сторінки
            try {
                const skip = page > 1 ? limit * (page - 1) : 0;

                // виконуємо запит на сервер
                const response = await fetch(`http://localhost:8000/api/post?skip=${skip}&limit=${limit}`);
                const { data } = await response.json();

                // оновлюємо стан
                setPosts(data.items);
                setCount(data.count);
            } catch (error) {
                console.error('Error fetching posts:', error);
            }            
        };

        loadPosts();
    }, [page]);


    // функція яка виконується при натисканні на кнопку "Наступна сторінка"
    // setPage - функція яка змінює поточну сторінку
    const nextPage = () => {
        // перевіряємо чи поточна сторінка менше за кількість сторінок
        if (page >= Math.ceil(count / limit)) {
            return;
        }
        setPage(page + 1);
    };

    // функція яка виконується при натисканні на кнопку "Попередня сторінка"
    // setPage - функція яка змінює поточну сторінку
    const prevPage = () => {
        // перевіряємо чи поточна сторінка дорівнює 1
        if (page === 1) {
            return;
        }
        setPage(page - 1);
    };


    return (
        
            <div className="App">
                <div className='container'>
                    <div id="posts-list">
                        {posts.map((post) => (
                            <div key={post.id} className='posts-item' >
                                <Link to={`/post/${post.id}`}>
                                    <h2 className="title">{post.title}</h2>
                                    <p className='text'>{post.content}</p>
                                </Link>                               
                            </div>
                        ))}
                    </div>
                    
                    <Link to="/create-post" className="btn">Додати новину</Link>
                
                    <div className="button-wrapper">
                        <button id="prev" onClick={prevPage} className='btn'><span>&larr;</span></button>
                        <span>{` ${page} `}</span>
                        <button id="next" onClick={nextPage} className='btn'><span>&rarr;</span></button>
                    </div>
                    
                </div>
            </div>        
    );
}

export default MainPage;