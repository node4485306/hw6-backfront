import { Router } from 'express';
import { BaseController } from '../controllers/base.controller';

// клас BaseRouter базовий для всіх маршрутів
class BaseRouter {
    // властивості
    // router - маршрутизаційний роутер
    public router: Router;
    // controller - контролер базового класу
    private controller: BaseController;

    // конструктор класу BaseRouter який приймає об'єкт класу BaseController з яким буде працювати маршрутизаційний роутер
    constructor(controller: BaseController) {
        this.router = Router();
        this.controller = controller;

        // ініціалізація маршрутів
        this.routes();
    }

    routes() {
        // створення маршрутів і під'єднання обробників подій для них
        this.router.route('/')
            .post(this.controller.create)
            .get(this.controller.getList);

        this.router.route('/:id')
            .get(this.controller.getSingle)
            .put(this.controller.update)
            .delete(this.controller.delete);
    }
}

export default BaseRouter
