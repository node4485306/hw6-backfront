import UsersSchema from './users'
import PostsSchema from './posts'

export default [
    UsersSchema,
    PostsSchema
]